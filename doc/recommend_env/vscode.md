# VSCode
VSCode 全称为 Visual Studio Code，是微软开发的开源、轻量、全功能的编辑器

VSCode 与 Visual Studio 不同，后者

- [官网](https://code.visualstudio.com/)
- [VSCode 教程 \| 极客教程](https://geek-docs.com/vscode/vscode-tutorials/what-is-vscode.html)

## 目录
- [VSCode](#vscode)
  - [目录](#目录)
  - [推荐理由](#推荐理由)
    - [通用](#通用)
    - [轻量化](#轻量化)
    - [上手容易](#上手容易)
    - [可扩展性强](#可扩展性强)
  - [安装](#安装)
    - [Windows](#windows)
    - [Ubuntu / Debian](#ubuntu--debian)
  - [插件](#插件)
  - [工作区](#工作区)
  - [SSH / WSL](#ssh--wsl)
  - [Git](#git)



---
## 推荐理由
### 通用
其一，VSCode 是一个全平台的编辑器，你可以在 Windows / Linux / MacOS 上使用它。甚至有组织开发了可以自部署的[浏览器版 VSCode](https://github.com/coder/code-server)，以供手机和平板使用。多设备间可通过微软账户自动同步设置和插件，非常方便。

其二，得益于丰富的插件，VSCode 几乎可以用于任意一种语言的开发，只需要准备好相应的后端开发环境、安装插件并进行简单的配置。

其三，利用 WSL 和 SSH 插件，VSCode 可以连接到远程主机进行开发，当需要在多台设备上开发同一个项目时，只需配置好远程连接，而无需重复配置虚拟机等后端环境。

### 轻量化
VSCode 对硬件的要求不高。[系统需求](https://code.visualstudio.com/docs/supporting/requirements)中要求仅有：
- 磁盘 > 500MB
- 内存 ~ 1GB
- CPU ~ 1.6GHz

在实际体验中，安装了33个插件以后，VSCode 比起 Visual Studio 和 Jetbrain 全家桶等 IDE，在启动、响应速度和电脑资源占用方面仍然好上不少，即使是我手边这台8年前的 Surface Pro 3 (i5) 也能无压力运行，实际内存占用~200MB

### 上手容易
VSCode 的界面和最基本的编辑功能就和我们熟悉的 Word 或者记事本相似，要开始使用并进行简单的编程是非常容易的。

### 可扩展性强
如前所述，VSCode 有丰富的插件，不仅可以对各种语言提供基本的语法高亮、语法检查等支持，还可以提供括号配对（新版本已内置）、Docker 容器可视化管理、http 服务器（辅助 web 开发）、Github Copilot 人工智能辅助编程等等诸多个性化功能。

在熟悉以后，用户甚至可以自行开发插件并发布到插件市场供人使用。



---
## 安装
### Windows

### Ubuntu / Debian



---
## 插件



---
## 工作区



---
## SSH / WSL



---
## Git
