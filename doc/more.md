# 学习更多
**下文中标有\*的内容为英文资料。**

## 目录
- [学习更多](#学习更多)
  - [目录](#目录)
  - [网站](#网站)
  - [视频](#视频)
  - [在线练习](#在线练习)
  - [致谢](#致谢)



---
## 网站
- [菜鸟教程](https://www.runoob.com/)：包括了 HTML、CSS、Javascript、Go、C/C++、Verilog 等各种语言的基础编程教程。也包含 Git、Linux 等常用环境和工具的教程。
- [极客教程](https://geek-docs.com/)
- [W3School](https://www.w3school.com.cn/)：web 开发教程。从基础的 HTML 到 CSS，乃至进阶的 XML、SQL、JS、PHP 和 ASP.NET。
- [C 语言中文网](http://c.biancheng.net/)：站如其名，提供了 C/C++ 的教程和实战案例等。也提供 Web 前端和 Linux 的教程。
- \*[The Missing Semester of Your CS Education](https://missing.csail.mit.edu/)：MIT 的 Linux 命令行、Vim、Git 等教程。页面底部有切换为中文的选项。
- [阮一峰的网络日志](https://www.ruanyifeng.com/blog/developer/)：业界大佬的博客，包括 Web 开发、Git、SSH、Bash 脚本、Docker、C 等方面的教程。
- [Linux 工具快速教程](https://linuxtools-rst.readthedocs.io/zh_CN/latest/)：Linux 下各种命令行工具的使用。
- [网道](https://wangdoc.com/)：C、Bash、SSH、Web 的文档
- [git-recipes](https://github.com/geeeeeeeeek/git-recipes/wiki)：Git使用说明
- [提问的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/main/README-zh_CN.md)：如何有效向他人提问
- \*[How-To-Ask-Questions-The-Smart-Way](http://www.catb.org/~esr/faqs/smart-questions.html)：上面的英文版
- [Linux 就该这么学](https://www.linuxprobe.com/)
- [Linux 命令大全（手册）](https://ipcmen.com/)：同时提供了几个在线尝试 Linux 命令的模拟器以供练习



---
## 视频
- \*[[MIT]计算机科学课堂中学不到的知识 The Missing Semester of Your CS Education(2020) | bilibili](https://www.bilibili.com/video/BV1x7411H7wa)：MIT 的 Linux 命令行、Vim、Git 等教程的视频版。



---
## 在线练习
- [Verilog OJ](https://verilogoj.ustc.edu.cn/oj/)：中国科学技术大学的 Verilog 在线练习平台



---
## 致谢
上面这些资料汇总自许多同学的推荐，以及计算机科学导论、计算机组成原理等课程的老师推荐。感谢他们。